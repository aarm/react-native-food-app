import React, { useState, useEffect } from "react";
import { ScrollView, Text, View, Image, TextInput } from "react-native";
import { StatusBar } from "expo-status-bar";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { BellIcon, MagnifyingGlassIcon } from "react-native-heroicons/outline";
import Categories from "../components/categories";
import axios from 'axios'
import Recipes from "../components/recipes";


const HomeScreen = () => {
    const [activeCategory , setActiveCategory] = useState("Beef")
    const [categories, setCategories] = useState([]);
    const [meals, setMeals] = useState([]);

    useEffect(()=>{
        getCategories();
        getRecipes();
      },[])

      const handleChangeCategory = category=>{
        getRecipes(category);
        setActiveCategory(category);
        setMeals([]);
      }
    

    const getCategories = async ()=>{
        try{
          const response = await axios.get('https://themealdb.com/api/json/v1/1/categories.php');
          if(response && response.data){
            setCategories(response.data.categories);
          }
        }catch(err){
          console.log('error: ',err.message);
        }
      }

      const getRecipes = async (category="Beef")=>{
        try{
          const response = await axios.get(`https://themealdb.com/api/json/v1/1/filter.php?c=${category}`);
          // console.log('got recipes: ',response.data);
          if(response && response.data){
            setMeals(response.data.meals);
          }
        }catch(err){
          console.log('error: ',err.message);
        }
      }
  return (
    <View className="flex-1 bg-white">
      <StatusBar style="dark" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 50 }}
        className="space-y-6 pt-14"
      >
        {/* /// Avatar and BellIcon */}
        <View className="mx-4 mb-2 flex-row justify-between items-center">
          <Image
            source={require("../assets/images/avatar.png")}
            style={{ height: hp(5), width: wp(10) }}
          />
          <BellIcon size={hp(4)} color="gray" />
        </View>

        {/* ///// Test */}
        <View className="mx-4 space-y-1 mb-1">
            <Text style={{fontSize: hp(1.7)}} className="text-neutral-600">Hello, Avinash</Text>
            <View>
                <Text style={{fontSize: hp(3.8)}} className="font-semibold text-neutral-600">Make your own food, </Text>
                <Text style={{fontSize: hp(3.8)}} className="font-semibold text-amber-600">stay at Home </Text>
            </View>
        </View>

        {/* ///Search bar */}
        <View className="mx-4 flex-row items-center rounded-full bg-black/20 p-[6px]">
            <TextInput
             placeholder="Search the item"
             placeholderTextColor="gray"
             style={{fontSize: hp(1.8)}}
             className="flex-1 text-base mb-1 pl-3 tracking-wider"
            />
            <View className="bg-white rounded-full p-3">
                <MagnifyingGlassIcon size={hp(2.5)} strokWidth={3} color="gray"/>
            </View>
        </View>

        {/* /// categries */}
        <View>
            <Categories categories={categories} handleChangeCategory={handleChangeCategory} activeCategory={activeCategory}/>
        </View>

        {/* Recipes */}
        <View>
            <Recipes meals={meals} categories={categories}/>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

import { StatusBar } from "expo-status-bar";
import React, { useEffect } from "react";
import { View , Text, Image } from "react-native";
import tw from 'twrnc'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import Animated, {useSharedValue, withSpring}  from "react-native-reanimated";
import { useNavigation } from "@react-navigation/native";

const WelcomeScreen = () => {
    const ring1 = useSharedValue(0)
    const ring2 = useSharedValue(0)
    const navigation = useNavigation()

    useEffect(() => {
        ring1.value = 0
        ring2.value = 0
        setTimeout(() => {
            ring1.value = withSpring(ring1.value + hp(5))
        }, 100);
        setTimeout(() => {
            ring2.value = withSpring(ring2.value + hp(5.5))
        }, 300);
        setTimeout(() => {
            navigation.navigate("Home")
        }, 2300);
    }, [])

    return (
        <View className="flex-1 justify-center items-center space-y-10 bg-amber-500">
        {/* // <View style={tw`flex-1 justify-center items-center space-y-10 bg-amber-500`}> */}
        <StatusBar style="dark"/>
        <Animated.View className="bg-white/20 rounded-full" style={{padding: ring2}}>
            <Animated.View className="bg-white/20 rounded-full " style={{padding: ring1}}>
                <Image source={require('../assets/images/welcome.png')} style={{width:wp(40), height: hp(20)}} />
            </Animated.View>
        </Animated.View>
    </View>
    )
}

export default  WelcomeScreen
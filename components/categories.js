import React from "react";
import { View, Text, ScrollView, TouchableOpacity, Image } from "react-native";
import { categoryData } from "../constant";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Animated, {FadeInDown} from 'react-native-reanimated'

export default function Categories({categories, handleChangeCategory, activeCategory}) {
  return (
    <Animated.View entering={FadeInDown.duration(500)}>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        className="space-x-4"
        contentContainerStyle={{ paddingHorizontal: 15 }}
      >
        {categories.map((cat, index) => {
            let active = cat.strCategory == activeCategory
            let activeClass = active ? "bg-amber-400" : 'bg-black/10'
          return (
            <TouchableOpacity
              key={index}
              onPress={() => handleChangeCategory(cat.strCategory)}
              className="flex items-center space-y-1 mr-2"
            >
              <View className={"rounded-full p-[6px] "+activeClass}>
                <Image
                  className="rounded-full"
                  source={{ uri: cat.strCategoryThumb }}
                  style={{ width: hp(5), height: hp(5) }}
                />
              </View>
              <Text className="text-neutral-600" style={{ fontSize: hp(1.6) }}>
                {cat.strCategory}
              </Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </Animated.View>
  );
}
